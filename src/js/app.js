const addBtn = document.querySelector('.add__items-btn');
const listToBuy = document.querySelector('.list-toBuy');
const finishBtn = document.querySelector('.finish__shopping');
const input = document.querySelector('#input');
const createListBtn = document.querySelector('.create__list');
let myList = [];
const localStorage = window.localStorage;
const close_modal = document.getElementById('close_modal');
const modal = document.getElementById('modal');
let li = document.querySelectorAll('.item-toBuy');

listToBuy.addEventListener('click', (e) => {

    if (e.target.classList.contains('bought')) {
        e.target.classList.toggle('hideIcon');
        e.target.nextElementSibling.classList.toggle('not-found-show');
        localStorage.removeItem(`'${e.target.closest('.item-toBuy').innerText}'`)

        let hiden = document.querySelectorAll('.hideIcon');
        hiden.forEach(i => {
            i.closest('li').classList.add('done');
        })


    } else if (e.target.classList.contains('not-found')) {

        if (e.target.previousElementSibling.classList.contains('hideIcon')) {
            e.target.previousElementSibling.classList.toggle('hideIcon');
            e.target.classList.toggle('not-found-show');
            e.target.closest('.item-toBuy').classList.toggle('done');
            localStorage.setItem(`'${e.target.closest('.item-toBuy').innerText}'`, `${e.target.closest('.item-toBuy').innerText}`);

            let hiden = document.querySelectorAll('.hideIcon');
            hiden.forEach(i => {
                i.closest('li').classList.add('done');
            })
        }
    } else if (e.target.classList.contains('remove__btn')) {
        localStorage.removeItem(`'${e.target.closest('.item-toBuy').innerText}'`)
        e.target.closest('.item-toBuy').classList.add('remove');
    }
})

    addBtn.onclick = function () {
        modal.classList.add('modal_vis');
        modal.classList.remove('bounceOutDown');
        input.value = '';
}
close_modal.onclick = function () {
    modal.classList.add('bounceOutDown');
    window.setTimeout(function () {
        modal.classList.remove('modal_vis');
    }, 500);
};


createListBtn.addEventListener('click', function (e) {
    myList.push(input.value.split('\n').join(' '));
    modal.classList.add('bounceOutDown');
    modal.classList.remove('modal_vis');
    const fullList = myList.join(' ').split(' ').filter(({length}) => length);

    for (let i = 0; i < fullList.length; i++) {

        let addItem = document.createElement('li');
        addItem.innerHTML = `${fullList[i]} <div class="item__options"> <img class="bought" width="40" height="30" src="./dist/img/ok-svg.png" alt="ok"> <img class="not-found" width="40" height="30" src="./dist/img/notOk.png" alt="ok"> <img class="remove__btn" width="20" height="25" src="./dist/img/recycle-bin.svg" alt="ok"></div>`;
        addItem.classList.add('item-toBuy');
        listToBuy.append(addItem);
        localStorage.setItem(`'${fullList[i]}'`, `${fullList[i]}`);
    }

    input.value = '';
    myList = [];
})

if (localStorage.length > 0) {
    for (let i = 0; i < localStorage.length; i++) {
        let key = localStorage.key(i).replace(/['"«»]/g, '');
        let addItem = document.createElement('li');
        addItem.innerHTML = `${key} <div class="item__options"> <img class="bought" width="40" height="30" src="./dist/img/ok-svg.png" alt="ok"> <img class="not-found" width="40" height="30" src="./dist/img/notOk.png" alt="ok"> <img class="remove__btn" width="20" height="25" src="./dist/img/recycle-bin.svg" alt="ok"></div>`;
        addItem.classList.add('item-toBuy');
        listToBuy.append(addItem);
    }
}

finishBtn.addEventListener('click', e => {
    const confirmToFinish = confirm('Are you sure you want to clear the list?');
    if(confirmToFinish) {
        localStorage.clear();
        document.querySelectorAll('.item-toBuy').forEach(e => e.classList.add('remove'));
        finishBtn.classList.toggle('block');
        addBtn.style.display = 'flex';
        input.value = '';
    }
})


